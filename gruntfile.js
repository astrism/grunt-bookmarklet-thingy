module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    nodeunit: {
      all: ['test/**/*test.js']
    },
    jshint: {
      files: ['grunt.js', 'tasks/**/*.js', 'test/**/*.js'],
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        node: true,
        es5: true
      },
      globals: {}
    },
    watch: {
      files: '<config:lint.files>',
      tasks: 'default'
    },
    bookmarklet: {
      options: {
        js: ['http://code.jquery.com/jquery-1.8.1.min.js','http://code.jquery.com/jquery-1.8.3.js'],
        jsIds: ['jquery-min','jquery'],
        css: ['http://static.jquery.com/files/rocker/css/reset.css'],
        body: 'test.js',
        out: 'bookmarklet.js',
        amdify: true,
        jshint: true,
        timestamp: true
      }
    }
  });

  // Load local tasks.
  grunt.loadTasks('tasks');

  // Load NPM Tasks
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', 'jshint', 'nodeunit', 'watch', 'bookmarklet');

};
